package com.example.limo2.fragmentacivitylifecycle;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

public class MainActivity extends BaseActivity {

    private FragmentA fragmentA;

    @Override
    protected void onStart() {
        super.onStart();
        Toast.makeText(this, "onStart activity", Toast.LENGTH_SHORT).show();
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toast.makeText(this, "onCreate activity", Toast.LENGTH_SHORT).show();

        fragmentA = FragmentA.getInstance();
        replaceCurrentFragment(fragmentA , true);


    }



    @Override
    protected void onStop() {
        super.onStop();
        Toast.makeText(this, "onStop activity", Toast.LENGTH_SHORT).show();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Toast.makeText(this, "onDestroy activity", Toast.LENGTH_SHORT).show();

    }

    @Override
    protected void onPause() {
        super.onPause();
        Toast.makeText(this, "onPause activity", Toast.LENGTH_SHORT).show();

    }

    @Override
    protected void onResume() {
        super.onResume();
        Toast.makeText(this, "onResume activity", Toast.LENGTH_SHORT).show();

    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Toast.makeText(this, "onRestart activity", Toast.LENGTH_SHORT).show();

    }

//    public void replaceCurrentFragment(Fragment targetFragment, boolean addToBackStack) {
//
//
//
//
//        FragmentManager manager = getSupportFragmentManager();
//
//        FragmentTransaction ft = manager.beginTransaction();
//
//        ft.replace(R.id.container, targetFragment, targetFragment.getClass()
//                .getName());
//        if (addToBackStack) {
//
//            ft.addToBackStack(targetFragment.getClass().getName());
//        }
//        ft.commit();
//
//
//
//
//    }
//
//    public void addFragment() {
//        FragmentA loginFragment = FragmentA.getInstance();
//        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
//        fragmentTransaction.replace(R.id.container, loginFragment);
//        fragmentTransaction.commit();
//    }
//
//    public void launchToMainActivity()
//    {
//        Intent intent=new Intent(MainActivity.this,MainActivity.class);
//        startActivity(intent);
//    }



    public  void setActionBar(String action)
    {

        setBackButtonEnabled(true);

        getSupportActionBar().setTitle(action);
    }


    public void setBackButtonEnabled(boolean isBackEnabled) {
        getSupportActionBar().setDisplayHomeAsUpEnabled(isBackEnabled);
        getSupportActionBar().setDisplayShowHomeEnabled(isBackEnabled);

    }


}
